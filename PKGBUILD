# Based on the file created for Arch Linux by:
# Tobias Powalowski <tpowa@archlinux.org>
# Thomas Baechler <thomas@archlinux.org>

# Maintainer (vd): torvic9

pkgbase=linux513-vd
pkgname=('linux513-vd' 'linux513-vd-headers')
_basekernel=5.13
_kernelname=-vd
_sub=16
#_rc=rc7
pkgver=${_basekernel}.${_sub}
pkgrel=1
#_stablequeue=6d399b408d
arch=('x86_64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc'
            'python' 'elfutils' 'git' 'libelf')
options=('!strip' '!ccache')
source=(git+https://gitlab.com/torvic9/linux-stable.git#tag=v${pkgver}${_kernelname}
    #git+https://gitlab.com/torvic9/linux-stable.git#branch=vd-${_basekernel}
    #
    # the main kernel config files
    'config.x86_64' 'config.x270' 'config.zen2' 'x509.genkey' "${pkgbase}.preset"
    # prepatch from stable-queue
    # prepatch-513-g6d399b408d.patch
    #
    # clang pgo and cfi support - experimental
    # 9001-clang-pgo-kees.patch
    # 9002-clang-cfi-tip-20210708.patch
    #
    # pgo profile data
    # vmlinux.profdata
    #
)

validpgpkeys=(
  'ABAF11C65A2970B130ABE3C479BE3E4300411886'  # Linus Torvalds
  '647F28654894E3BD457199BE38DBBDC86092693E'  # Greg Kroah-Hartman
)

sha256sums=('SKIP'
            '484b9b649f782995670c7a74d3b547ac66cc6036c7b5855e48f2ef075444569f'
            '1eedc0390fc851475f58b9d6d4221f214471d240fa919127321db342673136a4'
            'e2ce7f67da2e7bd22ebda0784ed3c289e25592c72f9e38003122cdc6df740e5a'
            'ab010dc5ef6ce85d352956e5996d242246ecd0912b30f0b72025c38eadff8cd5'
            '33635adfb2c79abf3fe961c1d7dcd613a04d716c7654cdd3c24fbf96ecc0ade7')

export KBUILD_BUILD_USER=$pkgbase
export KBUILD_BUILD_HOST=eos
export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

# signing
_signing=0
# edit the paths below to point to your signing keys
_key="$HOME/build/keys/vd513-kernel-key.pem"
_pubkey="$HOME/build/keys/vd513-kernel-pubkey.pem"

# custom clang path
# export PATH=/opt/clang13/bin:$PATH
_clang=0

if [[ ${_clang} -eq 1 ]]; then
	LLVMOPTS="LLVM=1 LLVM_IAS=1"
	CLANGOPTS="CC=clang LD=ld.lld"
else
	LLVMOPTS=""
	CLANGOPTS=""
fi

# Schedulers: cfs,bmq,pds
_sched="cfs"

if [[ ${_sched} == "cfs" ]]; then
    source+=('0100-tip-sched-core-20210716.patch')
    sha256sums+=('123bf1c51d3db62614fdf9d5aa6b344038aff135f652699affc58bc69a76c13a')
elif [[ ${_sched} == "bmq" || ${_sched} == "pds" ]]; then
    source+=('3001-projectc513-r0-vd.patch'
            '3002-projectc513-r1.patch'
            '3003-projectc513-r2.patch'
            '3004-projectc513-r3.patch')
    sha256sums+=('743cd365f0d15f45ff682d6552a796e50a6c8bc85f31ae4e6766bf73d2d9c480'
                '4a042834edc9e8e2b512d61fb410e33a494ef1e213ecd92295320709a00cea3a'
                '73aaf081010a87f56fe5083bc9c3fd4bd8a555ad371d407a7ecafae5b0c87b33'
                '867670a5b2bd50c177fe2fb3730c15e4264a6f1e7e10d9b64aaaefdd73822ba5')
else
	echo "Schedulers must be one of cfs, bmq or pds. Aborting." && exit 2
fi

TB=$(tput bold)
TN=$(tput sgr0)

prepare() {

  cd "${srcdir}/linux-stable"

  ./scripts/setlocalversion --save-scmversion
  echo "-${_kernelname/-/}" > localversion.10-pkgname
  echo "-${pkgrel}" > localversion.20-pkgrel

  echo -e "\n${TB}* APPLYING PATCHES${TN}"

  #echo -e "\n---- Reverts:" # add reverts here
  #patch -Rp1 -i "../8001-sched-fair-update_pick_idlest.revert"

  # apply patch from the source array (should be a pacman feature)
  local filename filename2
  for filename in "${source[@]}"; do
  	if [[ "$filename" =~ \.patch$ || "$filename" =~ \.diff$ ]]; then
		filename="${filename%%::*}"
		filename2="${filename#*-}"
        	echo -e "\n---- Applying patch ${TB}${filename2%%.*}${TN}:"
        	patch -Np1 -i "../${filename}"
  	fi
  done

  # kernel config
  echo -e "\n${TB}* KERNEL CONFIGURATION${TN}"
  local _config
  echo "---- Select configuration file:"
  echo "${TB}1)${TN} Default"
  echo "${TB}2)${TN} Zen2"
  echo "${TB}3)${TN} X270"
  while true ; do
  	read -p "Enter number (1-3): " _config
	  case ${_config} in
		1) cat ../config.x86_64 > ./.config && break ;;
		2) cat ../config.zen2 > ./.config && break ;;
		3) cat ../config.x270 > ./.config && break ;;
		*) echo "Please enter a number (1-3)!" && continue ;;
  	  esac
  done

  if [[ ${_signing} -eq 1 ]] ; then
    cat ${_key} > ./certs/vd513-kernel-key.pem
    cat ${_pubkey} > ./certs/vd513-kernel-pubkey.pem
    sed -i "s|signing_key|vd513-kernel-key|" ./.config
    sed -ri "s|^(CONFIG_SYSTEM_TRUSTED_KEYS=).*|\1\"certs/vd513-kernel-pubkey.pem\"|" ./.config
  else
    cat ../x509.genkey > ./certs/x509.genkey
  fi

  #make $LLVMOPTS prepare
  make $LLVMOPTS olddefconfig
  if [[ ${_sched} == "bmq" || ${_sched} == "pds" ]]; then
    ./scripts/config -e CONFIG_SCHED_ALT
    [[ ${_config} -eq 2 ]] && ./scripts/config -d CONFIG_CPU_FREQ_DEFAULT_GOV_SCHEDUTIL && \
      ./scripts/config -e CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND
    [[ ${_sched} == "bmq" ]] && ./scripts/config -e CONFIG_SCHED_BMQ
    [[ ${_sched} == "pds" ]] && ./scripts/config -e CONFIG_SCHED_PDS
  fi
  # get kernel version
  make $LLVMOPTS -s kernelrelease > version
  printf "\n  Prepared %s version %s\n" "$pkgbase" "$(<version)"
  read -p "---- Enter 'y' for nconfig: " NCONFIG
  [[ $NCONFIG == "y" ]] && make $LLVMOPTS nconfig

  # rewrite configuration
  yes '' | make $LLVMOPTS config >/dev/null
}

build() {
  cd "${srcdir}/linux-stable"

  # copy pgo profile data
  # cp $srcdir/vmlinux.profdata ./

  # build!
  make $LLVMOPTS LOCALVERSION= bzImage modules
  # below cmd is for using a pgo profile, 1st without LTO, 2nd with LTO
  # make $LLVMOPTS KCFLAGS=-fprofile-use=vmlinux.profdata LOCALVERSION= bzImage modules
  # make $LLVMOPTS KCFLAGS=-lto-cs-profile-file=vmlinux.profdata LOCALVERSION= bzImage modules

  # build turbostat
  # make $CLANGOPTS -C tools/power/x86/turbostat
}

package_linux513-vd() {
  pkgdesc="The ${pkgbase/linux/Linux} vd kernel and modules"
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio>=27')
  optdepends=('crda: to set the correct wireless channels of your country')
  provides=(VIRTUALBOX-GUEST-MODULES)
  replaces=(linux513-vd-virtualbox-guest-modules)

  cd "${srcdir}/linux-stable"

  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make $CLANGOPTS -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "${pkgbase}" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  # make room for external modules
  local _extramodules="extramodules-${pkgbase}"
  ln -s "../${_extramodules}" "$modulesdir/extramodules"
  # add real version for building modules and running depmod from hook
  echo "${kernver}" |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modules/${_extramodules}/version"

  echo -e "\n${TB}* INSTALLING MODULES${TN}"
  make $CLANGOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" INSTALL_MOD_STRIP=1 modules_install
  # do not strip when building a pgo kernel
  # make $CLANGOPTS LOCALVERSION= INSTALL_MOD_PATH="${pkgdir}/usr" modules_install

  # remove build and source links
  rm $modulesdir/source
  rm $modulesdir/build
  [[ -f ./certs/vd513-kernel-key.pem ]] && rm ./certs/vd513-kernel-key.pem

  # add mkinitcpio preset (not strictly needed)
  install -Dm644 "$srcdir/${pkgbase}.preset" "$pkgdir/etc/mkinitcpio.d/${pkgbase}.preset"
}

package_linux513-vd-headers() {
  pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} vd kernel"

  cd "${srcdir}/linux-stable"
  local kernver="$(<version)"
  local _builddir="${pkgdir}/usr/lib/modules/${kernver}/build"

  echo -e "\n${TB}* INSTALLING HEADERS${TN}"
  install -Dt "${_builddir}" -m644 Makefile .config Module.symvers System.map version vmlinux localversion.* || exit 32
  install -Dt "${_builddir}/kernel" -m644 kernel/Makefile
  install -Dt "${_builddir}/arch/x86" -m644 "arch/x86/Makefile"
  #mkdir "${_builddir}/.tmp_versions"

  cp -t "${_builddir}" -a include scripts

  # add objtool for external module building and enabled VALIDATION_STACK option
  install -Dt "${_builddir}/tools/objtool" tools/objtool/objtool

  # add turbostat
  # install -Dt "${_builddir}/tools/turbostat" tools/power/x86/turbostat/turbostat

  # add xfs and shmem for aufs building
  mkdir -p "${_builddir}"/{fs/xfs,mm}

  cp -t "${_builddir}/arch/x86" -a "arch/x86/include"
  install -Dt "${_builddir}/arch/x86/kernel" -m644 "arch/x86/kernel/asm-offsets.s"

  install -Dt "${_builddir}/drivers/md" -m644 drivers/md/*.h
  install -Dt "${_builddir}/net/mac80211" -m644 net/mac80211/*.h

  # http://bugs.archlinux.org/task/13146
  install -Dt "${_builddir}/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # http://bugs.archlinux.org/task/20402
  install -Dt "${_builddir}/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "${_builddir}/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "${_builddir}/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # copy in Kconfig files
  find . -name Kconfig\* -exec install -Dm644 {} "${_builddir}/{}" \;

  # remove unneeded stuff
  echo -e "\n${TB}* REMOVING UNNEEDED FILES${TN}"
  # remove unneeded architectures
  local _arch
  for _arch in "${_builddir}"/arch/*/; do
    [[ ${_arch} == */x86/ ]] && continue
    rm -r "${_arch}"
  done

  # remove files already in linux-docs package
  rm -r "${_builddir}/Documentation"

  # remove broken symlinks
  find -L "${_builddir}" -type l -printf 'Removing %P\n' -delete

  # remove loose objects"
  find "${_builddir}" -type f -name '*.o' -printf 'Removing %P\n' -delete

  # strip scripts directory
  echo -e "\n${TB}* STRIPPING${TN}"
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "${_builddir}" -type f -perm -u+x ! -name vmlinux -print0)
  
  strip -v $STRIP_STATIC "${_builddir}/vmlinux"
  
  echo -e "\n${TB}* SYMLINKING${TN}"
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$_builddir" "$pkgdir/usr/src/$pkgbase"
}
